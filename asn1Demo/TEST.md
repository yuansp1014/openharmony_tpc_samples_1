﻿## asn1单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/PeculiarVentures/ASN1.js/tree/master/test) 进行单元测试

**单元测试用例覆盖情况**

### asn1

| 接口名                                   | 是否通过 | 备注 |
|---------------------------------------| -------- | ---- |
| Universal.Sequence      | pass     |      |
| Universal.Integer             | pass     |      |
| Universal.Null  | pass     |      |
| Universal.Bool       | pass     |      |
| Universal.PrintableString | pass     |      |
| JSONSerializer | pass     |      |
| JSONDeserializer  | pass     |      |
| DERSerializer  | pass     |      |
| DERDeserializer  | pass     |      |
