## path-to-regexp单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/pillarjs/path-to-regexp/blob/master/src/index.spec.ts) 进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|parse|pass||
|compile|pass||
|tokensToFunction|pass||
|match|pass||
|regexpToFunction|pass||
|tokensToRegexp|pass||
|pathToRegexp|pass||