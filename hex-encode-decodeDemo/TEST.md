# hex-encode-decode单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/tiaanduplessis/hex-encode-decode/blob/master/test.js)进行单元测试

单元测试用例覆盖情况

|   接口名    |是否通过	|备注|
|:--------:|:---:|:---:|
| decode() |pass||
| encode() |pass ||