## pcx-js单元测试用例

该测试用例基于OpenHarmony环境,采用[原库测试用例](https://github.com/mathiasbynens/utf8.js/blob/master/tests/tests.js) 进行单元测试。

### 单元测试用例覆盖情况

| 接口名       | 是否通过 | 备注        |
|-----------|---|-----------|
|utf8.encode|pass| utf8的编码能力 |
|utf8.decode|pass| utf8的解码能力 |