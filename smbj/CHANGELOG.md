## 2.0.0
- 修复偶现身份认证失败的问题

## 2.0.0-rc.0
- DevEco Studio 版本： 4.1 Canary(4.1.3.317)
- OpenHarmony SDK:API11 (4.1.0.36)
- ArkTS新语法适配

## 1.0.1

1. 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）。

## 1.0.0

- 详细功能：
  1. 局域网文件共享
  2. 文件读写
  3. 读文件夹内容
  4. 增删文件夹