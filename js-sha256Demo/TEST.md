# js-sha256单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/emn178/js-sha256/tree/master/tests)进行单元测试

单元测试用例覆盖情况

|   接口名    |是否通过	|备注|
|:--------:|:---:|:---:|
|    sha256()    |pass||
|    sha224()    |pass||
|    sha256.hex()    |pass||
|  sha256.digest()   |pass||
|   sha256.array()   |pass||
