## complex.js单元测试用例

该测试用例基于OpenHarmony系统下，参照[原库测试用例](https://github.com/infusion/Complex.js/tree/master/tests/complex.test.js) 进行单元测试

### 单元测试用例覆盖情况

| 接口名                          | 是否通过  |备注|
|------------------------------|-------|---|
| add(a: AValue, b?: BValue)   | pass  |
| div(a: AValue, b?: BValue)   | pass  |
| sub(a: AValue, b?: BValue)   | pass  |
| mul(a: AValue, b?: BValue)   | pass  |
| pow(a: AValue, b?: BValue)   | pass  |
| sqrt()                       | pass  |
| abs()                        | pass  |
| sin()                        | pass  |
| cos()                        | pass  |
| tan()                        | pass  |
| sec()                        | pass  |
| csc()                        | pass  |
| cot()                        | pass  |
| acos()                       | pass  |
| floor(places: number)        | pass  |
| ceil(places: number)         | pass  |
| round(places: number)        | pass  |
| clone()                      | pass  |
| isZero()                     | pass  |