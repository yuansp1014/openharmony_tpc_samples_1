## 2.0.1
- 修改xmldom的引入方式，将相对路径引入改为版本号引入

## 2.0.0

- 适配DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK:API11 (4.1.0.36)
- 新语法适配
- 使用GlobalContext替换globalthis

## 1.0.2

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## v1.0.1

- 修复解压压缩的问题
- api8升级到api9，并转换为stage模型

## v1.0.0

- epublib 读写电子书功能均已实现
