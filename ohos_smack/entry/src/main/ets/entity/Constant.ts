/**
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 *
 * This software is distributed under a license. The full license
 * agreement can be found in the file LICENSE in this distribution.
 * This software may not be copied, modified, sold or distributed
 * other than expressed in the named license agreement.
 *
 * This software is distributed without any warranty.
 */

export class Constant {
  static HOST_IP: string = "106.15.92.248"
  static HOST_DOMAIN: string = "iZuf6axz08c30hqse971u4Z"
  static SERVICE_NAME: string = "conference"
  static HOST_RES: string = "/gloox"
  static HOST_PORT: number = 5222
}





